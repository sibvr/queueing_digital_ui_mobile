
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:queueing_digital/emailtoken_page.dart';
import 'package:queueing_digital/login_page.dart';
import 'package:queueing_digital/signup_page.dart';
import 'package:queueing_digital/home_page.dart';
import 'package:queueing_digital/splash_screen.dart';
import 'package:queueing_digital/question_help.dart';
import 'package:queueing_digital/forgot_password.dart';




void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/',
      routes: {
        '/forgotpassword': (context) => ForgotPassword(),
        '/signup': (context) => SignupPage(),
        '/verifyemail': (context) => EmailToken(),
        '/login': (context) => LoginPage(),
        '/home': (context) => Home(),
        '/questionHelp': (context) => QuestionHelp(),
      },
      title: 'QUEUEING DIGITAL APPS',
      theme: ThemeData(
          textTheme: GoogleFonts.quicksandTextTheme(
            Theme.of(context).textTheme,
          )),
      home: SplashScreen(),
      builder: EasyLoading.init(),
    );
  }
}
