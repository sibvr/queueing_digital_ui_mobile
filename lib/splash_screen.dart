
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'dart:async';


class SplashScreen extends StatefulWidget {
  @override
  Splash createState() => Splash();
}

class Splash extends State<SplashScreen>  {

  @override
  void initState() {
    super.initState();

  }
  
  @override
  Widget build(BuildContext context) {
        Timer(
            Duration(seconds: 3),
                () =>
                 Navigator.pushReplacementNamed(context, '/login'));
    
    return MaterialApp(
      home: Scaffold(
        body: Container(
          width: MediaQuery.of(context).size.width,       
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                // Color(0xFF9face6),
                // Color(0xFF7CD1DD),
                Color.fromRGBO(159, 172, 230, 1),Color.fromRGBO(224, 195, 252, 1)
              ],                 
            ),
          ),

          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Spacer(),
              Align(
                alignment: Alignment.center,
                child: Icon(Icons.headset, 
                  size: 100,
                  color: Colors.white,
                  ),
              ),
              Spacer(),
            ],
          ),
        ),
      ),
    );
  }
}
