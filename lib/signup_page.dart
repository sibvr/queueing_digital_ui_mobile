import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'dart:convert';
import 'package:flutter_session/flutter_session.dart';
import 'package:queueing_digital/emailtoken_page.dart';

final fullnameController = TextEditingController();
final usernameController = TextEditingController();
final emailController = TextEditingController();
final passwordController = TextEditingController();

Future navigateTo(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => EmailToken()));
}

postRegister() async {

  //http parameter
  String url = "http://192.168.43.198:8881/bvnextgen/api/registerService";
  Map<String, String> headers = {"Content-type": "application/json"};
  Map<String, dynamic> toJson() => {
    'fullname' : fullnameController.text,
    'username' : usernameController.text,
    'email' : emailController.text,
    'password' : passwordController.text,
  };
  String rawJson = jsonEncode(toJson());
  print(rawJson);
  //http request
  Response response = await post(url, headers: headers, body: rawJson);

  //response data
  var serviceReturn = jsonDecode(response.body);
  var serviceContent = serviceReturn[0]['serviceContent'];
  var status = serviceContent['status'];

  if (response.statusCode==200)
  {
    print('success, status: ' + status);
    if(status == "S"){
      print("Masuk Session");
      var session = FlutterSession();
      await session.set("usernameReg", usernameController.text);
      await session.set("passwordReg", passwordController.text);
      await session.set("emailReg", emailController.text);
      await session.set("fullnameReg", fullnameController.text);
    }
    return status;
  }
  else
  {
    print('error not 200');
  }
}

class SignupPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold( 
      resizeToAvoidBottomInset: true,
      resizeToAvoidBottomPadding: false,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height/4,
              decoration: BoxDecoration (
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    // Color(0xFF9face6),
                    // Color(0xFF7CD1DD),
                      Color.fromRGBO(159, 172, 230, 1),Color.fromRGBO(224, 195, 252, 1)
                  ],                 

                ),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(90)
                )
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Spacer(),
                
                  Align(
                    alignment: Alignment.bottomRight,
                    child: Padding(
                      padding: const EdgeInsets.only(
                        bottom: 30,
                        right: 30
                      ),
                      child: Text('Create An Account',
                        style: TextStyle(
                          fontSize: 26,
                          color: Colors.white,
                          fontWeight: FontWeight.w600
                        ), 
                      ),
                    ),
                  ),
                ],
              ),
            ),
           
           Container(
             width: MediaQuery.of(context).size.width,
             height: MediaQuery.of(context).size.height/1.5,
             padding: EdgeInsets.only(top: 62),
             child: Column(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width/1.2,
                    height: 50,
                    padding: EdgeInsets.only(
                      top: 4, left: 15, right: 15, bottom: 4
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(50)
                      ),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                        color: Colors.black26,
                        blurRadius: 10
                        ) 
                      ]  
                    ),
                    child: TextField(
                      controller: fullnameController,
                      decoration: InputDecoration(
                        icon: Icon(Icons.person, color: Colors.grey,),
                        hintText: 'Full Name',
                        border: InputBorder.none
                      )
                    )
                  ),
     
                  Container(
                    width: MediaQuery.of(context).size.width/1.2,
                    height: 50,
                    margin: EdgeInsets.only(top: 32),
                    padding: EdgeInsets.only(
                      top: 4, left: 15, right: 15, bottom: 4
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(50)
                      ),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                        color: Colors.black26,
                        blurRadius: 10
                        ) 
                      ]  
                    ),
                    child: TextField(
                      controller: usernameController,
                      decoration: InputDecoration(
                        icon: Icon(Icons.person, color: Colors.grey,),
                        hintText: 'Username',
                        border: InputBorder.none
                      )
                    )  
                  ),  

                  Container(
                    width: MediaQuery.of(context).size.width/1.2,
                    height: 50,
                    margin: EdgeInsets.only(top: 32),
                    padding: EdgeInsets.only(
                      top: 4, left: 15, right: 15, bottom: 4
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(50)
                      ),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                        color: Colors.black26,
                        blurRadius: 10
                        ) 
                      ]  
                    ),
                    child: TextField(
                      controller: emailController,
                      decoration: InputDecoration(
                        icon: Icon(Icons.email, color: Colors.grey,),
                        hintText: 'Email',
                        border: InputBorder.none
                      )
                    )  
                  ),  

                  Container(
                    width: MediaQuery.of(context).size.width/1.2,
                    height: 50,
                    margin: EdgeInsets.only(top: 32),
                    padding: EdgeInsets.only(
                      top: 4, left: 15, right: 15, bottom: 4
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(50)
                      ),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                        color: Colors.black26,
                        blurRadius: 10
                        ) 
                      ]  
                    ),
                    child: TextField(
                      controller: passwordController,
                      decoration: InputDecoration(
                        icon: Icon(Icons.vpn_key, color: Colors.grey,),
                        hintText: 'Password',
                        border: InputBorder.none
                      ),
                      obscureText: true,
                    )  
                  ),  
                  Spacer(),

                  Container(
                    height: MediaQuery.of(context).size.height/16,
                    width: MediaQuery.of(context).size.width/1.2,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(50)
                      ),
                      gradient: LinearGradient(
                        // begin: Alignment.topCenter,
                        // end: Alignment.bottomCenter,
                        colors: [
                          // Color(0xFF7CD1DD),
                          // Color(0xFF9face6),   
                            Color.fromRGBO(159, 172, 230, 1),Color.fromRGBO(224, 195, 252, 1)                      
                        ], 
                      )
                    ),

                     child: Center(
                      child: FlatButton(
                          onPressed: () async {
                            await EasyLoading.show(status:"Mengirim kode verifikasi");
                            var status = await postRegister();
                            if(status=="S"){
                              print('Scaffold S');
                              navigateTo(context);
                            }
                            else{
                              EasyLoading.showError('Registrasi gagal');
                              print('Scaffold F');
                            }
                          },
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(18.0),
                          
                        ),

                        // textColor: Colors.white,
                        child: Text('SIGN UP',
                          style: TextStyle( 
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w900,
                          )
                        )
                      ),
                    )
                  ),

                  new InkWell(
                    child: Align(
                    alignment: Alignment.center,
                    child: Padding(
                      padding: const EdgeInsets.only(
                        top:20, 
                      ),
                      
                      child: Text('Already have an account? Log in', 
                        style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600, color: Colors.grey[600])                                                                
                        ),
                      ),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, "/login");
                    },
                  ),
                ],
              )
            )
          ]
        )
      )
    );
  }
}
