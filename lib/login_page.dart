import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart';
import 'dart:convert';
import 'package:flutter_session/flutter_session.dart';
import 'package:queueing_digital/forgot_password.dart';
import 'package:queueing_digital/home_page.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

final usernameController = TextEditingController();
final passwordController = TextEditingController();


Future navigateTo(context) async {
  var session = FlutterSession();
  await session.set("type", "cs");
  Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
}

Future navigateForgotPassword(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => ForgotPassword()));
}

postLogin() async {

  //http parameter
  String url = "http://192.168.43.198:8881/bvnextgen/api/loginWithoutRoleService";
  Map<String, String> headers = {"Content-type": "application/json"};
  Map<String, dynamic> toJson() => {
    'username' : usernameController.text,
    'password' : passwordController.text
  };
  String rawJson = jsonEncode(toJson());

  //http request
  Response response = await post(url, headers: headers, body: rawJson);

  //response data
  var serviceReturn = jsonDecode(response.body);
  var serviceContent = serviceReturn[0]['serviceContent'];
  var status = serviceContent['status'];

  if (response.statusCode==200)
  {
    print('success, status: ' + status);
    if(status == "S"){
      var session = FlutterSession();
      await session.set("username", serviceContent['username']);
      await session.set("email", serviceContent['email']);
      await session.set("password", passwordController.text);
      await session.set("token", serviceContent['access_token']);
    }
    return status;
  }
  else
  {
    print('error not 200');
  }
}

class LoginPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      resizeToAvoidBottomPadding: false,     
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
      reverse: true,
        child: Column(
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height/2.5,
              decoration: BoxDecoration (
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      // Color(0xFF9face6),
                      // Color(0xFF7CD1DD),
                      Color.fromRGBO(159, 172, 230, 1),Color.fromRGBO(224, 195, 252, 1)
                    ],
                  ),
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(90)
                  )
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Spacer(),
                  Align(
                    alignment: Alignment.center,
                    child: Icon(Icons.headset,
                      size: 100,
                      color: Colors.white,
                    ),
                  ),
                  Spacer(),

                  Align(
                    alignment: Alignment.bottomRight,
                    child: Padding(
                      padding: const EdgeInsets.only(
                          bottom: 30,
                          right: 30
                      ),
                      child: Text('Hello there!',
                        style: TextStyle(
                            fontSize: 26,
                            color: Colors.white,
                            fontWeight: FontWeight.w800
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),

            Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height/2,
                padding: EdgeInsets.only(top: 62),
                
                child: Column(
                  children: <Widget>[
                    Container(
                        width: MediaQuery.of(context).size.width/1.2,
                        height: 50,
                        padding: EdgeInsets.only(
                            top: 4, left: 15, right: 15, bottom: 4
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                                Radius.circular(50)
                            ),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black26,
                                  blurRadius: 10
                              )
                            ]
                        ),
                        child: TextField(
                            controller: usernameController,
                            decoration: InputDecoration(
                                icon: Icon(Icons.person, color: Colors.grey,),
                                hintText: 'Email or Username',
                                border: InputBorder.none
                            )
                        )
                    ),

                    Container(
                        width: MediaQuery.of(context).size.width/1.2,
                        height: 50,
                        margin: EdgeInsets.only(top: 32),
                        padding: EdgeInsets.only(
                            top: 4, left: 15, right: 15, bottom: 4
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                                Radius.circular(50)
                            ),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black26,
                                  blurRadius: 10
                              )
                            ]
                        ),
                        child: TextField(
                            obscureText: true,
                            controller: passwordController,
                            decoration: InputDecoration(
                                icon: Icon(Icons.vpn_key, color: Colors.grey,),
                                hintText: 'Password',
                                border: InputBorder.none
                            )
                        )
                    ),

                    Align(
                      alignment: Alignment.centerRight,
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top:20, right: 34
                        ),
                        child: GestureDetector(
                          onTap: () async {
                            navigateForgotPassword(context);
                          },
                          child: Text(
                            "Forgot Password",
                            style: TextStyle(
                              color: Colors.grey[600],
                            ),
                          ),
                        ),
                      ),
                    ),

                    Spacer(),

                    Container(

                        height: 50,
                        width: MediaQuery.of(context).size.width/1.2,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                                Radius.circular(50)
                            ),
                            gradient: LinearGradient(
                              colors: [
                                // Color(0xFF7CD1DD),
                                // Color(0xFF9face6),
                                Color.fromRGBO(159, 172, 230, 1),Color.fromRGBO(224, 195, 252, 1)
                              ],
                            )
                        ),
                        child: Center(
                          child: FlatButton(
                              onPressed: () async {
                                await EasyLoading.show(status: "Loading");
                                var status = await postLogin();
                                if(status=="S"){
                                  navigateTo(context);
                                }
                                else{
                                  EasyLoading.showError("Login gagal");
                                  print('Scaffold F');
                                }
                              },
                              shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(18.0),

                              ),

                              // textColor: Colors.white,
                              child: Text('LOGIN',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20,
                                    fontWeight: FontWeight.w900,
                                  )
                              )
                          ),
                        )
                    ),

                    new InkWell(

                      child: Align(
                        alignment: Alignment.center,
                        child: Padding(
                          padding: const EdgeInsets.only(
                            top:20,
                          ),

                          child: Text('Dont have an account? Sign up',
                              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600, color: Colors.grey[600])
                          ),
                        ),
                      ),
                      onTap: () {
                        Navigator.pushNamed(context, "/signup");
                      },
                    ),
                  ]
                )
            )
          ]
        )
      )
    );
  }
}


