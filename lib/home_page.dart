import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'dart:convert';
import 'package:flutter_session/flutter_session.dart';

import 'package:queueing_digital/login_page.dart';
import 'package:queueing_digital/question_help.dart';
import 'package:queueing_digital/services/get_queue_service.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

Future navigateLogOut(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => LoginPage()));
}

Future navigateQuestion(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => QuestionHelp()));
}

postQueue(type) async {

  var token = await FlutterSession().get("token");
  var email = await FlutterSession().get("email");

  //http parameter
  String url = "http://192.168.43.198:8881/bvnextgen/api/generateQueueNumberTellerService";
  Map<String, String> headers = {"Content-type": "application/json",'Authorization': 'Bearer $token'};
  Map<String, dynamic> toJson() => {
    'email' : email,
    'type' : type
  };
  String rawJson = jsonEncode(toJson());

  //http request
  Response response = await post(url, headers: headers, body: rawJson);

  //response data
  var serviceReturn = jsonDecode(response.body);
  var serviceContent = serviceReturn[0]['serviceContent'];
  var status = serviceContent['status'];

  if (response.statusCode==200)
  {
    print('success, status: ' + status);
    return status;
  }
  else
  {
    print('error not 200');
  }
}

postLogOut() async {

  var token = await FlutterSession().get("token");

  //http parameter
  String url = "http://192.168.43.198:8881/bvnextgen/api/logoutService";
  Map<String, String> headers = {"Content-type": "application/json",'Authorization': 'Bearer $token'};
  Map<String, dynamic> toJson() => {
      'empty': true,
      'additionalProp1': {},
      'additionalProp2': {},
      'additionalProp3': {}
  };
  String rawJson = jsonEncode(toJson());

  //http request
  Response response = await post(url, headers: headers, body: rawJson);

  //response data
  var serviceReturn = jsonDecode(response.body);
  var serviceContent = serviceReturn[0]['serviceContent'];
  var status = serviceContent['status'];

  if (response.statusCode==200)
  {
    print('success, status: ' + status);
    return status;
  }
  else
  {
    print('error not 200');
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  List<String> lst = ['CS', 'Teller'];
  int selectedIndex = 0;

  //from service
  String myQueue;
  List counter = [];
  List counterNumber = [];
  String type = "CS";

   getType() async{
     var typeSession = await FlutterSession().get("type");
     getQueueNumberInit(typeSession);
  }

  void getQueueNumberInit(String typeParam) async{
    GetQueue instance = GetQueue(type:typeParam);
    await instance.getMyQueueNumber();
    await instance.getCurrentQueueNumber();
    setState(() {
      myQueue = instance.myQueue;
      counter = instance.counter;
      counterNumber = instance.counterNumber;
      if(typeParam=="teller" || typeParam == "Teller"){
        print("Masuk sini if else");
        selectedIndex = 1;
        type="Teller";
      }
      else{
        selectedIndex = 0;
        type = "CS";
      }
      EasyLoading.dismiss();
    });
  }

  void changeIndex(int choose){
    if(choose == 0){
      type="CS";
    }
    else{
      type="Teller";
    }
    getQueueNumberInit(type);
  }

  @override
  void initState() {
    super.initState();
    EasyLoading.show(status:'Loading');
    getType();
  }

  @override
  Widget build(BuildContext context) {

  return Scaffold(
    body: Container(
      height: double.infinity,
      decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [Color.fromRGBO(159, 172, 230, 1),Color.fromRGBO(224, 195, 252, 1)])
      ),
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.fromLTRB(20, 40, 20, 50),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              Text(
                                'Hello,',
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.white,
                                ),
                              ),
                              Text(
                                'User!',
                                style: TextStyle(
                                  fontSize: 40,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                ),
                              ),
                              Text(
                                'Silakan menunggu',
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.white,

                                ),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          child:Row(
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child: Text(''),
                              ),
                              Expanded(
                                flex:1,
                                child: ConstrainedBox(
                                  constraints: BoxConstraints.tightFor(
                                      width:50,
                                      height:50
                                  ),
                                  child: ElevatedButton(
                                    child: Text(
                                      '?',
                                      style:TextStyle(
                                        color: Color.fromRGBO(159, 172, 230, 1),
                                        fontSize: 25,
                                      ),
                                    ),
                                    onPressed:() {
                                      navigateQuestion(context);
                                    },
                                    style: ElevatedButton.styleFrom(
                                      primary: Colors.white,
                                      shape: CircleBorder(),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex:1,
                                child: ConstrainedBox(
                                  constraints: BoxConstraints.tightFor(
                                      width:50,
                                      height:50
                                  ),
                                  child: ElevatedButton(
                                    child: Icon(
                                      Icons.exit_to_app,
                                      color: Color.fromRGBO(159, 172, 230, 1),
                                    ),
                                    onPressed: () async {
                                      var status = await postLogOut();
                                      if(status=="S"){
                                        print("scaffold S");
                                        navigateLogOut(context);
                                      }
                                      else{
                                        print('Scaffold F');
                                      }
                                    },
                                    style: ElevatedButton.styleFrom(
                                      primary: Colors.white,
                                      shape: CircleBorder(),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),


                  ],
                ),
              ),
              // ========================== PILIHAN LIHAT ANTRIAN CS ATAU  TELLER  ======================================
              Padding(
                padding: EdgeInsets.fromLTRB(5, 20, 5, 0),
                child:Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(child: customRadio(lst[0],0)),
                    SizedBox(width: 10),
                    Expanded(child: customRadio(lst[1],1)),
                  ],
                ),
              ),
              // =======================================================================================================================
              SizedBox(

                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(10, 15, 10, 15),
                    child: Column(
                      children:<Widget>[myQueue != "000"
                        //=====================================================================================
                        ?Column(
                          children: <Widget>[
                            Center(
                              child: Text(
                                'Tujuan',
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.grey[600],
                                ),
                              ),
                            ),
                            SizedBox(height:10),
                            Text(
                              '$type',
                              style: TextStyle(
                                fontSize: 30,
                                fontWeight: FontWeight.bold,
                                color: Color.fromRGBO(136, 119, 252, 1),
                              ),
                            ),
                            SizedBox(height:10),
                            Center(
                              child: Text(
                                'Nomor antrianmu',
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.grey[600],
                                ),
                              ),
                            ),
                            Text(
                              "$myQueue",
                              style: TextStyle(
                                fontSize: 60,
                                fontWeight: FontWeight.bold,
                                color: Color.fromRGBO(136, 119, 252, 1),
                              ),
                            ),
                            SizedBox(height: 20),
                          ],
                        )
                        :Column(
                          children: <Widget>[
                            SizedBox(height:20),
                            Text(
                              'Anda belum ambil antrian',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w600,
                                color: Colors.grey[600],
                              ),
                            ),
                            SizedBox(height:20),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children:<Widget>[
                                FlatButton(
                                    color: Color.fromRGBO(136, 119, 252, 1),
                                    onPressed: () async {
                                      var status = await postQueue(type);
                                      if(status=="S"){
                                        print("masuk scaffold setelah generate");
                                        print("scaffold type" + type);
                                        getQueueNumberInit(type);
                                        print("myQueue sekarang: "+ myQueue);
                                      }
                                      else{
                                        print('Scaffold F');
                                      }
                                    },
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.all(10.0),
                                      child: Text('Ambil Antrian',
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 20,
                                            fontWeight: FontWeight.w900,
                                          )
                                      ),
                                    )
                                ),
                              ],
                            ),
                            SizedBox(height:20),
                          ],
                        ),
                        //=====================================================================================
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                flex: 3,
                                child: Text(
                                  '',
                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: Text(
                                  'Counter',
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.grey[600],
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: Text(
                                  'Antrian',
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.grey[600],
                                  ),
                                ),
                              ),
                            ]
                        ),
                        ListView.builder(
                          scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                          itemCount: counter.length,
                          itemBuilder: (context,index) {
                            return Card(
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(0, 5, 0, 10),
                                child: Row(
                                  children: <Widget> [
                                    Expanded(
                                      flex: 2,
                                      child:Center(
                                        child: Text(
                                          '$type',
                                          style: TextStyle(
                                            fontSize: 25,
                                            fontWeight: FontWeight.w600,
                                            color: Colors.grey[600],
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 1,
                                      child:Center(
                                        child: Text(
                                          counter[index],
                                          style: TextStyle(
                                            fontSize: 30,
                                            fontWeight: FontWeight.w600,
                                            color: Colors.grey[600],
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 2,
                                      child:Center(
                                        child: Text(
                                          counterNumber[index],
                                          style: TextStyle(
                                            fontSize: 40,
                                            fontWeight: FontWeight.w700,
                                            color: Color.fromRGBO(206, 139, 240, 1),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          }
                        ),
                        SizedBox(height: 5),
                        // Card(
                        //   child: Padding(
                        //     padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                        //     child: Row(
                        //       children: <Widget> [
                        //         Expanded(
                        //           flex: 2,
                        //           child:Center(
                        //             child: Text(
                        //               'Teller',
                        //               style: TextStyle(
                        //                 fontSize: 25,
                        //                 fontWeight: FontWeight.w600,
                        //                 color: Colors.grey[600],
                        //               ),
                        //             ),
                        //           ),
                        //         ),
                        //         Expanded(
                        //           flex: 1,
                        //           child:Center(
                        //             child: Text(
                        //               '2',
                        //               style: TextStyle(
                        //                 fontSize: 30,
                        //                 fontWeight: FontWeight.w600,
                        //                 color: Colors.grey[600],
                        //               ),
                        //             ),
                        //           ),
                        //         ),
                        //         Expanded(
                        //           flex: 2,
                        //           child:Center(
                        //             child: Text(
                        //               'C38',
                        //               style: TextStyle(
                        //                 fontSize: 40,
                        //                 fontWeight: FontWeight.w700,
                        //                 color: Color.fromRGBO(206, 139, 240, 1),
                        //               ),
                        //             ),
                        //           ),
                        //         ),
                        //       ],
                        //     ),
                        //   ),
                        // ),
                        // SizedBox(height: 5),
                        // Card(
                        //   child: Padding(
                        //     padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                        //     child: Row(
                        //       children: <Widget> [
                        //         Expanded(
                        //           flex: 2,
                        //           child:Center(
                        //             child: Text(
                        //               'CS',
                        //               style: TextStyle(
                        //                 fontSize: 25,
                        //                 fontWeight: FontWeight.w600,
                        //                 color: Colors.grey[600],
                        //               ),
                        //             ),
                        //           ),
                        //         ),
                        //         Expanded(
                        //           flex: 1,
                        //           child:Center(
                        //             child: Text(
                        //               '2',
                        //               style: TextStyle(
                        //                 fontSize: 30,
                        //                 fontWeight: FontWeight.w600,
                        //                 color: Colors.grey[600],
                        //               ),
                        //             ),
                        //           ),
                        //         ),
                        //         Expanded(
                        //           flex: 2,
                        //           child:Center(
                        //             child: Text(
                        //               'C38',
                        //               style: TextStyle(
                        //                 fontSize: 40,
                        //                 fontWeight: FontWeight.w700,
                        //                 color: Color.fromRGBO(206, 139, 240, 1),
                        //               ),
                        //             ),
                        //           ),
                        //         ),
                        //       ],
                        //     ),
                        //   ),
                        // ),

                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    ),


    floatingActionButton: Visibility(
      visible: myQueue =="000"?false:true,
      child: FloatingActionButton(
        onPressed: () async {
          await EasyLoading.show(status:'Ambil antrian...');
          var status = await postQueue(type);
          if(status=="S"){
            print("masuk scaffold setelah generate");
            print("scaffold type" + type);
            getQueueNumberInit(type);
            print("myQueue sekarang: "+ myQueue);
          }
          else{
            print('Scaffold F');
          }
        },
        backgroundColor: Colors.white,
        child: Icon(
                Icons.add,
                color: Color.fromRGBO(159, 172, 230, 1),
            ),
      ),
    ),
    // bottomNavigationBar: CurvedNavigationBar(
    //   color: Colors.white,
    //   backgroundColor: Color.fromRGBO(159, 172, 230, 1),
    //   buttonBackgroundColor: Colors.white,
    //   height:60,
    //   items:<Widget>[
    //     Icon(Icons.home, size: 30),
    //     Icon(Icons.add,size: 30),
    //     Icon(Icons.list,size: 30),
    //   ],
    //   animationDuration: Duration(
    //     milliseconds: 300
    //   ),
    //   onTap: (index) {
    //     debugPrint('Current Index is $index');
    //   },
    // ),
  );
  }

  Widget customRadio(String txt, int choose){
    return RaisedButton(
      onPressed: () async {
        await EasyLoading.show(status:'loading...');
        changeIndex(choose);
      },
      shape:RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      color: selectedIndex == choose ? Color.fromRGBO(136, 119, 252, 1) : Colors.white,
      child: Text(txt,style: TextStyle(fontSize:18, color: selectedIndex == choose ? Colors.white : Color.fromRGBO(136, 119, 252, 1))),
    );
  }
}


