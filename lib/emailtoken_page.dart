
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'dart:convert';
import 'package:queueing_digital/numericpad.dart';
import 'package:flutter_session/flutter_session.dart';
import 'package:queueing_digital/login_page.dart';


Future navigateTo(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => LoginPage()));
}

postEmailToken(String code) async {

  var usernameReg = await FlutterSession().get("usernameReg");
  var passwordReg = await FlutterSession().get("passwordReg");

  //http parameter
  String url = "http://192.168.43.198:8881/bvnextgen/api/activationUserWithCodeService";
  Map<String, String> headers = {"Content-type": "application/json"};
  Map<String, dynamic> toJson() => {
    'username' : usernameReg,
    'password' : passwordReg,
    'confirmationCode': code,
  };
  String rawJson = jsonEncode(toJson());
  print(rawJson);
  //http request
  Response response = await post(url, headers: headers, body: rawJson);

  //response data
  var serviceReturn = jsonDecode(response.body);
  var serviceContent = serviceReturn[0]['serviceContent'];
  var status = serviceContent['status'];

  if (response.statusCode==200) {
    print('success, status: ' + status);
    return status;
  }
  else {
    print('error not 200');
  }
}

postResendCode() async {

  var emailReg = await FlutterSession().get("emailReg");
  var fullnameReg = await FlutterSession().get("fullnameReg");

  //http parameter
  String url = "http://192.168.43.198:8881/bvnextgen/api/resendActivationCodeService";
  Map<String, String> headers = {"Content-type": "application/json"};
  Map<String, dynamic> toJson() => {
    'email' : emailReg,
    'fullname' : fullnameReg,
  };
  String rawJson = jsonEncode(toJson());
  print(rawJson);
  //http request
  Response response = await post(url, headers: headers, body: rawJson);

  //response data
  var serviceReturn = jsonDecode(response.body);
  var serviceContent = serviceReturn[0]['serviceContent'];
  var status = serviceContent['status'];

  if (response.statusCode==200)
  {
    print('success, status: ' + status);
    if(status == "S"){
      print('Masuk S');
    }
    return status;
  }
  else
  {
    print('error not 200');
  }
}


class EmailToken extends StatefulWidget {

  // final String phoneNumber;

  // EmailToken({@required this.phoneNumber});

  @override
  _EmailTokenState createState() => _EmailTokenState();
}

class _EmailTokenState extends State<EmailToken> {

  String code = "";
  void initState(){
    EasyLoading.showInfo("Kode terkirim");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            size: 30,
            color: Colors.black,
          ),
        ),
        title: Text(
          "Verify Email",
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        centerTitle: true,
        textTheme: Theme.of(context).textTheme,
      ),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[

            Expanded(
              child: Container(
                color: Colors.white,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[

                          buildCodeNumberBox(code.length > 0 ? code.substring(0, 1) : ""),
                          buildCodeNumberBox(code.length > 1 ? code.substring(1, 2) : ""),
                          buildCodeNumberBox(code.length > 2 ? code.substring(2, 3) : ""),
                          buildCodeNumberBox(code.length > 3 ? code.substring(3, 4) : ""),

                        ],
                      ),
                    ),

                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 14),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          
                          Text(
                            "Didn't receive code? ",
                            style: TextStyle(
                              fontSize: 18,
                              color: Color(0xFF818181),
                            ),
                          ),
                          
                          SizedBox(
                            width: 8,
                          ),

                          GestureDetector(
                            onTap: () async {
                              var status = await postResendCode();
                              if(status=="S"){
                                print('Scaffold S');
                              }
                              else{
                                print('Scaffold F');
                              }
                            },
                            child: Text(
                              "Request again",
                              style: TextStyle(
                                fontSize: 18,
                              ),
                            ),
                          ),
                          
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),

            Container(
              height: MediaQuery.of(context).size.height * 0.12,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(25),
                ),
              ),
              child: Padding(
                padding: EdgeInsets.all(16),
                child: Row(
                  children: <Widget>[

                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          print("Verify and Create Account");
                        },
                        child: code.length ==4 ? Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                            // begin: Alignment.topCenter,
                            // end: Alignment.bottomCenter,
                            colors: [
                              // Color(0xFF7CD1DD),
                              // Color(0xFF9face6),
                                Color.fromRGBO(159, 172, 230, 1),Color.fromRGBO(224, 195, 252, 1)
                            ],
                          ),

                            borderRadius: BorderRadius.all(
                              Radius.circular(50),
                            ),
                          ),

                          child: Center(
                            child: FlatButton(
                                onPressed: () async {
                                  await EasyLoading.show(status: "Verifikasi email");
                                  var status = await postEmailToken(code);
                                  if(status=="S"){
                                    await EasyLoading.showSuccess("Email berhasil diverifikasi");
                                    navigateTo(context);
                                  }
                                  else{
                                    print('Scaffold F');
                                  }
                                },
                              shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(18.0),

                              ),

                              // textColor: Colors.white,
                              child: Text(
                              "Verify and Create Account",
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white
                                )
                              )
                            ),
                          )

                          // child: Center(
                          //   child: Text(
                          //     "Verify and Create Account",
                          //     style: TextStyle(
                          //       fontSize: 20,
                          //       fontWeight: FontWeight.bold,
                          //       color: Colors.white
                          //     ),
                          //   ),
                          // ),
                        ) : Container()
                      ),
                    ),

                  ],
                ),
              ),
            ),

            NumericPad(
              onNumberSelected: (value) {
                print(value);
                setState(() {
                  if(value != -1){
                    if(code.length < 4){
                      print("masuk kurang 4");
                      code = code + value.toString();
                    }
                  }
                  else{
                    code = code.substring(0, code.length - 1);
                  }
                  print(code);        
                });
              },
            ),

          ],
        )
      ),
    );
  }

  Widget buildCodeNumberBox(String codeNumber) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 8),
      child: SizedBox(
        width: 60,
        height: 60,
        child: Container(
          decoration: BoxDecoration(
            color: Color(0xFFF6F5FA),
            borderRadius: BorderRadius.all(
              Radius.circular(15),
            ),
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.black26,
                  blurRadius: 25.0,
                  spreadRadius: 1,
                  offset: Offset(0.0, 0.75)
              )
            ],
          ),
          child: Center(
            child: Text(
              codeNumber,
              style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.bold,
                color: Color(0xFF1F1F1F),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

