import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'dart:convert';
import 'package:queueing_digital/login_page.dart';


final emailForgotController = TextEditingController();

Future navigateTo(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => LoginPage()));
}

postForgotPassword() async {

  //http parameter
  String url = "http://192.168.43.198:8881/bvnextgen/api/forgotPasswordService";
  Map<String, String> headers = {"Content-type": "application/json"};
  Map<String, dynamic> toJson() => {
    'email' : emailForgotController.text
  };
  String rawJson = jsonEncode(toJson());
  print(rawJson);
  //http request
  Response response = await post(url, headers: headers, body: rawJson);

  //response data
  var serviceReturn = jsonDecode(response.body);
  var serviceContent = serviceReturn[0]['serviceContent'];
  var status = serviceContent['status'];

  if (response.statusCode==200)
  {
    print('success, status: ' + status);
    if(status == "S"){
      print('Masuk S');
    }
    return status;
  }
  else
  {
    print('error not 200');
  }
}

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: true,
        resizeToAvoidBottomPadding: false,
        body: SingleChildScrollView(
            child: Column(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height/2.5,
                    decoration: BoxDecoration (
                        gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                            // Color(0xFF9face6),
                            // Color(0xFF7CD1DD),
                            Color.fromRGBO(159, 172, 230, 1),Color.fromRGBO(224, 195, 252, 1)
                          ],

                        ),
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(90)
                        )
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Spacer(),
                        Align(
                          alignment: Alignment.bottomRight,
                          child: Padding(
                            padding: const EdgeInsets.only(
                                bottom: 30,
                                right: 30
                            ),
                            child: Text('Forgot Password',
                              style: TextStyle(
                                  fontSize: 26,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),

                  Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height/3,
                      padding: EdgeInsets.only(top: 60),
                      child: Column(
                        children: <Widget>[
                          Container(
                              width: MediaQuery.of(context).size.width/1.2,
                              height: 50,
                              margin: EdgeInsets.only(top: 32),
                              padding: EdgeInsets.only(
                                  top: 4, left: 15, right: 15, bottom: 4
                              ),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(50)
                                  ),
                                  color: Colors.white,
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.black26,
                                        blurRadius: 10
                                    )
                                  ]
                              ),
                              child: TextField(
                                  controller: emailForgotController,
                                  decoration: InputDecoration(
                                      icon: Icon(Icons.email, color: Colors.grey,),
                                      hintText: 'Email',
                                      border: InputBorder.none
                                  )
                              )
                          ),
                          Spacer(),

                          Container(
                              height: MediaQuery.of(context).size.height/16,
                              width: MediaQuery.of(context).size.width/1.2,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(50)
                                  ),
                                  gradient: LinearGradient(
                                    colors: [
                                      Color.fromRGBO(159, 172, 230, 1),Color.fromRGBO(224, 195, 252, 1)
                                    ],
                                  )
                              ),

                              child: Center(
                                child: FlatButton(
                                    onPressed: () async {
                                      await EasyLoading.show(status:"Periksa Password");
                                      var status = await postForgotPassword();
                                      if(status=="S"){
                                        EasyLoading.showSuccess('Password berhasil dikirim');
                                        print('Scaffold S');
                                        navigateTo(context);
                                      }
                                      else{
                                        EasyLoading.showError('Kirim password gagal');
                                        print('Scaffold F');
                                      }
                                    },
                                    shape: RoundedRectangleBorder(
                                      borderRadius: new BorderRadius.circular(18.0),

                                    ),
                                    // textColor: Colors.white,
                                    child: Text('Send Password',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 20,
                                          fontWeight: FontWeight.w900,
                                        )
                                    )
                                ),
                              )
                          ),
                        ],
                      )
                  )
                ]
            )
        )
    );
  }
}
