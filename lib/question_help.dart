import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_session/flutter_session.dart';
import 'package:queueing_digital/home_page.dart';
import 'package:queueing_digital/services/get_question_service.dart';

Future navigateHome(context,param) async {
  var session = FlutterSession();
  await session.set("type", param);
  Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
}

class QuestionHelp extends StatefulWidget {
  @override
  _QuestionHelpState createState() => _QuestionHelpState();
}

class _QuestionHelpState extends State<QuestionHelp> {

  List questionList = [];
  List queueingDestination = [];

  void getQuestionInit() async{
    print('masuk');
    GetQuestion instance = GetQuestion();
    await instance.getQuestion();
    setState(() {
      questionList = instance.question;
      queueingDestination = instance.queueingDestination;
    });
    EasyLoading.dismiss();
  }

  @override
  void initState() {
    super.initState();
    EasyLoading.show(status:'loading...');
    getQuestionInit();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [Color.fromRGBO(159, 172, 230, 1),Color.fromRGBO(224, 195, 252, 1)])
        ),
        child: Padding(
          padding: EdgeInsets.fromLTRB(20, 60, 20, 50),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Text(
                'Question',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 45,
                  fontWeight: FontWeight.w600,
                  color: Colors.white,
                ),
              ),
              Text(
                'Help',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 45,
                  fontWeight: FontWeight.w600,
                  color: Colors.white,
                ),
              ),
              SizedBox(height: 30),
              Expanded(
                child:ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: questionList.length,
                  itemBuilder:(context,index) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        GestureDetector(
                          onTap:() {
                            navigateHome(context, queueingDestination[index]);
                          },
                          child:Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child:Padding(
                              padding: EdgeInsets.fromLTRB(10, 20, 10, 20),
                              child: Text(
                                questionList[index],
                                style:TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w600,
                                  color: Color.fromRGBO(136, 119, 252, 1),
                                ),
                              ),
                            ),
                          )
                        ),
                        SizedBox(height: 10),
                      ],
                    );
                  }
                ),
              ),
            ],
          ),
    ),
      ),
    );
  }
}
