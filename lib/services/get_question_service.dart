import 'package:http/http.dart';
import 'dart:convert';
import 'package:flutter_session/flutter_session.dart';

class GetQuestion {
  List<String> queueingDestination = List<String>();
  List<String> question = List<String>();

  GetQuestion();

  Future <void> getQuestion() async {
    print('masuk sini');

    var token = await FlutterSession().get("token");
    var userId = await FlutterSession().get("username");

    //http parameter
    String url = 'http://192.168.43.198:8881/bvnextgen/api/getQuestionHelpService?userId=$userId';
    Map<String, String> headers = {"Content-type": "application/json",'Authorization': 'Bearer $token'};
    //http request
    Response response = await get(url, headers: headers);

    //response data
    var serviceReturn = jsonDecode(response.body);
    var serviceContent = serviceReturn[0]['serviceContent'];
    var status = serviceContent['status'];

    print(serviceContent['questionList'][0]['question']);

    for(int i=0; i<serviceContent['questionList'].length;i++){
      question.add(serviceContent['questionList'][i]['question']);
      queueingDestination.add(serviceContent['questionList'][i]['queueingDestination']);
    }

    if (response.statusCode==200) {
      print('success, status: ' + status);
      print(question);
      print(queueingDestination);
    }
    else {
      print('error not 200');
    }
  }
}

