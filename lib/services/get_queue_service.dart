import 'package:http/http.dart';
import 'dart:convert';
import 'package:flutter_session/flutter_session.dart';

class GetQueue {
  String email;
  String type;
  List<String> counterNumber = List<String>();
  List<String> counter = List<String>();
  String currentQueue;
  String myQueue;

  GetQueue({this.email,this.type});

  Future <void> getMyQueueNumber() async {

    var token = await FlutterSession().get("token");
    email = await FlutterSession().get("email");

    //http parameter
    String url = 'http://192.168.43.198:8881/bvnextgen/api/inquiryQueueNumberTellerByEmailService?email=$email&type=$type';
    Map<String, String> headers = {"Content-type": "application/json",'Authorization': 'Bearer $token'};
    print(url);
    //http request
    Response response = await get(url, headers: headers);

    //response data
    var serviceReturn = jsonDecode(response.body);
    var serviceContent = serviceReturn[0]['serviceContent'];
    var status = serviceContent['status'];
    myQueue = serviceContent['queueNumberTeller'];

    if (response.statusCode==200) {
      print('success, status: ' + status);
      print('type: ' + type);
      print('myQueue: ' + myQueue);
    }
    else {
      print('error not 200');
    }
  }

  Future <void> getCurrentQueueNumber() async {
  print('Masuk sini');
    var token = await FlutterSession().get("token");

    //http parameter
    String url = 'http://192.168.43.198:8881/bvnextgen/api/getCurrentQueueNumberActiveService?type=$type';
    Map<String, String> headers = {"Content-type": "application/json",'Authorization': 'Bearer $token'};

    //http request
    Response response = await get(url, headers: headers);

    //response data
    var serviceReturn = jsonDecode(response.body);
    var serviceContent = serviceReturn[0]['serviceContent'];
    var status = serviceContent['status'];
    if(serviceContent['counterQueue'].length>0){
      counterNumber = [];
      counter = [];
      for(int i=0;i<serviceContent['counterQueue'].length;i++){
        counterNumber.add(serviceContent['counterQueue'][i]['counterNumber']);
        counter.add(serviceContent['counterQueue'][i]['counter']);
      }
      print(counterNumber);
      print(counter);
    }
    else {
      counterNumber.add('-');
      counter.add('-');
    }

    if (response.statusCode==200) {
      print('success, status: ' + status);
      // print('currentQueue: ' + currentQueue);
    }
    else {
      print('error not 200');
    }
  }
}





