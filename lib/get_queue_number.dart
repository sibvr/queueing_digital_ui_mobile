import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'dart:convert';
import 'package:flutter_session/flutter_session.dart';

import 'package:queueing_digital/home_page.dart';
import 'package:queueing_digital/signup_page.dart';

Future navigateTo(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
}

final emailQueueController = TextEditingController();

postQueue(type) async {

  var token = await FlutterSession().get("token");

  //http parameter
  String url = "http://192.168.43.198:8881/bvnextgen/api/generateQueueNumberTellerService";
  Map<String, String> headers = {"Content-type": "application/json",'Authorization': 'Bearer $token'};
  Map<String, dynamic> toJson() => {
    'email' : emailQueueController.text,
    'type' : type
  };
  String rawJson = jsonEncode(toJson());

  //http request
  Response response = await post(url, headers: headers, body: rawJson);

  //response data
  var serviceReturn = jsonDecode(response.body);
  var serviceContent = serviceReturn[0]['serviceContent'];
  var status = serviceContent['status'];

  if (response.statusCode==200)
  {
    print('success, status: ' + status);
    return status;
  }
  else
  {
    print('error not 200');
  }
}

class GetQueueNumber extends StatefulWidget {

  @override
  _GetQueueNumberState createState() => _GetQueueNumberState();
}

class _GetQueueNumberState extends State<GetQueueNumber> {

  List<String> lst = ['CS', 'Teller'];
  int selectedIndex = 0;
  String type;

  void changeIndex(int index){
    if(index == 0){
      type="CS";
    }
    else{
      type="Teller";
    }
    print(type);
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [Color.fromRGBO(159, 172, 230, 1),Color.fromRGBO(224, 195, 252, 1)])
        ),
        child:Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: <Widget>[
              SizedBox(height: 70),
              Text(
                'Silakan ambil nomor antrian',
                style: TextStyle(
                  fontSize: 35,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(height:40),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                      Radius.circular(20)
                  ),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black26,
                        blurRadius: 20
                    ),
                  ],
                ),
                child:Padding(
                  padding: EdgeInsets.fromLTRB(20, 40, 20, 50),
                  child: Column(
                    children:<Widget>[
                      Text(
                        'Tujuan anda',
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w600,
                          color: Colors.grey[600],
                        ),
                      ),
                      SizedBox(height: 20),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(child: customRadio(lst[0],0)),
                          SizedBox(width: 10),
                          Expanded(child: customRadio(lst[1],1)),
                        ],
                      ),
                      SizedBox(height: 40),
                      Text(
                        'Email anda',
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w600,
                          color: Colors.grey[600],
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width/1.2,
                        height: 50,
                        margin: EdgeInsets.only(top: 20),
                        padding: EdgeInsets.only(
                            top: 4, left: 15, right: 15, bottom: 4
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                              Radius.circular(50)
                          ),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                                color: Color.fromRGBO(136, 119, 252, 1),
                                blurRadius: 2
                            ),
                          ],
                        ),
                        child: TextField(
                          controller: emailQueueController,
                          decoration: InputDecoration(
                              icon: Icon(Icons.email, color: Colors.grey,),
                              hintText: 'Email',
                              border: InputBorder.none
                          ),
                        ),
                      ),
                      SizedBox(height: 50),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children:<Widget>[
                          FlatButton(
                              color: Color.fromRGBO(136, 119, 252, 1),
                              onPressed: () async {
                                var status = await postQueue(type);
                                if(status=="S"){
                                  navigateTo(context);
                                }
                                else{
                                  print('Scaffold F');
                                }
                              },
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              child: Padding(
                                padding: EdgeInsets.all(10.0),
                                child: Text('Daftar',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20,
                                      fontWeight: FontWeight.w900,
                                    )
                                ),
                              )
                          ),
                        ],
                      ),

                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget customRadio(String txt, int index){
    return RaisedButton(
      onPressed: () => changeIndex(index),
      shape:RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      color: selectedIndex == index ? Color.fromRGBO(136, 119, 252, 1) : Colors.white,
      child: Text(txt,style: TextStyle(fontSize:18 ,color: selectedIndex == index ? Colors.white : Color.fromRGBO(136, 119, 252, 1))),
    );
  }
}




